//
//  FRDetailViewController.h
//  FaroDemo
//
//  Created by Jorge Leonardo Monge García on 22/06/14.
//  Copyright (c) 2014 Jorge Leonardo Monge García. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FRDetailViewController : UIViewController

@property (strong, nonatomic) id detailItem;

@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
@end
