//
//  FRBeaconViewController.h
//  FaroDemo
//
//  Created by Jorge Leonardo Monge García on 12/07/14.
//  Copyright (c) 2014 Jorge Leonardo Monge García. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FRBaseBeaconManagerViewController.h"
#import "ESTBeacon.h"


typedef enum : int
{
    ESTScanTypeBluetooth,
    ESTScanTypeBeacon
    
} ESTScanType;

@interface FRBeaconViewController : FRBaseBeaconManagerViewController

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@end
