//
//  FRBeaconViewController.m
//  FaroDemo
//
//  Created by Jorge Leonardo Monge García on 12/07/14.
//  Copyright (c) 2014 Jorge Leonardo Monge García. All rights reserved.
//

#import "FRBeaconViewController.h"
#import "ESTBeaconManager.h"
#import "FRAppDelegate.h"

//####### Includes de pruebas #######//
#import "FRStoreRepository.h"
#import "FRBeaconRepository.h"
//####### Includes de pruebas #######//


@interface FRBeaconViewController () <ESTBeaconManagerDelegate>

@property (nonatomic, copy)     void (^completion)(ESTBeacon *);
@property (nonatomic, assign)   ESTScanType scanType;

@property (nonatomic, strong) ESTBeaconManager *beaconManager;
@property (nonatomic, strong) ESTBeaconRegion *region;
@property (nonatomic, strong) NSArray *beaconsArray;

@property (nonatomic, strong) NSArray *beaconsList;

@end

@implementation FRBeaconViewController

@synthesize beaconsList = _beaconsList;
@synthesize beaconManager = _beaconManager;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSUserDefaults *myUserDefaults = [NSUserDefaults standardUserDefaults];

    NSLog(@"Ultima Notificacion: %@", [myUserDefaults objectForKey:@"ultimaNot"]);
//    [self initBeaconManager];
    
    //Inicializacion del MOC:
    FRAppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    _managedObjectContext      = [appDelegate managedObjectContext];
    
    FRStoreRepository *storeRepo = [[FRStoreRepository alloc] initWithManagedObjectContext:_managedObjectContext];
    
    [storeRepo syncStoresFromServer];
    
    FRBeaconRepository *beaconRepo = [[FRBeaconRepository alloc] initWithManagedObjectContext:_managedObjectContext];
    
    [beaconRepo syncBeaconsFromServer];
    
}

-(void)didRangeBeacon:(ESTBeacon *)beacon inRegion:(ESTBeaconRegion *)region andBeaconManager:(ESTBeaconManager *)manager
{
    NSLog(@"TENGO LA NOTIFICACION de la region: %@", region.identifier);
}

-(NSArray *)getBeaconsList
{
    if (_beaconsList == nil)
    {
        NSUUID *uuid = [[NSUUID alloc]initWithUUIDString:@"B9407F30-F5F8-466E-AFF9-25556B57FE6D"];
        NSUUID *uuidv = [[NSUUID alloc]initWithUUIDString:@"08D4A950-80F0-4D42-A14B-D53E063516E6"];
        _beaconsList = [[NSArray alloc] initWithObjects:
                        [[ESTBeaconRegion alloc] initWithProximityUUID: uuid major:32795 minor:12381 identifier:@"Blueberry Pie"],
                        [[ESTBeaconRegion alloc] initWithProximityUUID: uuid major:60806 minor:6366 identifier:@"Icy Marshmallow"],
                        [[ESTBeaconRegion alloc] initWithProximityUUID: uuidv major:61001 minor:13001 identifier:@"Virtual"]
                        , nil];
    }
    
    return _beaconsList;
}

-(void)initBeaconManager
{
    _beaconManager = [[ESTBeaconManager alloc] init];
    [_beaconManager setDelegate:self];
    
    NSArray *beacons = [self getBeaconsList];
    for (ESTBeaconRegion *beacon in beacons) {
    
        [_beaconManager startRangingBeaconsInRegion:beacon];
        [self.beaconManager startMonitoringForRegion:beacon];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ESTBeaconManager delegate

-(void) beaconManager:(ESTBeaconManager *)manager didExitRegion:(ESTBeaconRegion *)region
{
    NSLog(@"Salio de la region");
}

-(void)beaconManager:(ESTBeaconManager *)manager didStartMonitoringForRegion:(CLRegion *)region
{
    NSLog(@"Entro");
}

- (void)beaconManager:(ESTBeaconManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(ESTBeaconRegion *)region
{
    NSLog(@"Beacons %d", [beacons count]);
    if (beacons.count > 0)
    {
        ESTBeacon *firstBeacon = [beacons firstObject];
        NSLog(@"Region: %@", region.identifier);
        NSLog(@"Becon major: %@", firstBeacon.major);
        NSLog(@"Becon minor: %@", firstBeacon.minor);
        NSLog(@"Becon proximidad: %@", [self textForProximity:firstBeacon.proximity]);
        
        
//        self.zoneLabel.text     = [self textForProximity:firstBeacon.proximity];
//        self.imageView.image    = [self imageForProximity:firstBeacon.proximity];
    }
}

- (NSString *)textForProximity:(CLProximity)proximity
{
    switch (proximity) {
        case CLProximityFar:
            return @"Far";
            break;
        case CLProximityNear:
            return @"Near";
            break;
        case CLProximityImmediate:
            return @"Immediate";
            break;
            
        default:
            return @"Unknown";
            break;
    }
}

//- (void)beaconManager:(ESTBeaconManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(ESTBeaconRegion *)region
//{
//    self.beaconsArray = beacons;
//}

- (void)beaconManager:(ESTBeaconManager *)manager didDiscoverBeacons:(NSArray *)beacons inRegion:(ESTBeaconRegion *)region
{
    NSLog(@"Entro en didDiscoverBeacons");
    return;
    self.beaconsArray = beacons;
}

- (void)dealloc
{
    NSLog(@"DEALLOC");
    [_beaconManager stopRangingBeaconsInAllRegions];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
