//
//  FRSynchronize.m
//  FaroDemo
//
//  Created by Jorge Leonardo Monge García on 03/08/14.
//  Copyright (c) 2014 Jorge Leonardo Monge García. All rights reserved.
//

#import "FRSynchronize.h"
#import "FRStoreRepository.h"

@implementation FRSynchronize

@synthesize managedObjectContext = _managedObjectContext;

-(id)initWithManagedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
    self = [super init];
    
    if(self)
    {
        _managedObjectContext = managedObjectContext;
    }
    
    return self;
}


-(void)dealloc
{
    _managedObjectContext = nil;
}

@end
