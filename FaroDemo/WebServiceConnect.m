//
//  WebServiceConnect.m
//  Bayer
//
//  Created by Jorge Leonardo Monge Garcia on 12/27/12.
//  Copyright (c) 2012 Jorge Leonardo Monge García. All rights reserved.
//

#import "WebServiceConnect.h"

@implementation WebServiceConnect

////////////////////////////////////////////////////////////////////////////////////////////////////////
/*                                  CONECCION CON WEB SERVICE                                         */
////////////////////////////////////////////////////////////////////////////////////////////////////////

-(void)processRequestWhitURL:(NSString *)url sender:(id)sender requestType:(int)type andBlock:(void (^)(NSMutableData *Data, int type, int status, id sender))processResponse
{
    
    callbackBlock = processResponse;
    _type         = type;
    _sender       = sender;
    NSURL           *urlRequest    = [NSURL URLWithString:url];
    NSURLRequest    *request       = [NSURLRequest requestWithURL:urlRequest];
    NSURLConnection *connection    = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    //_receivedData = [[NSMutableData alloc] init]; //Fix Memory Leak
    
    if(!connection)
    {
        NSLog(@"Error de conexion");
        callbackBlock(nil, _type, 0, _sender);
        return;
    }
    
}

#pragma mark NSURLConnectionDelegate methods

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    if (_receivedData == NULL) {
        _receivedData = [[NSMutableData alloc] init];
    }
    [_receivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [_receivedData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    connection = nil;
    _receivedData = nil;
    NSLog(@"Error de conexion");
    callbackBlock(nil, _type, 0, _sender);
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{

    callbackBlock(_receivedData, 0, 1, _sender);
    
    _receivedData = nil;// Fix Memory Leak
    connection = nil;
    
}




@end
