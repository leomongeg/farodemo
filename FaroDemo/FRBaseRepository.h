//
//  FRBaseRepository.h
//  FaroDemo
//
//  Created by Jorge Leonardo Monge García on 03/08/14.
//  Copyright (c) 2014 Jorge Leonardo Monge García. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FRBaseRepository : NSObject

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

- (id)initWithManagedObjectContext:(NSManagedObjectContext *) managedObjectContext;

-(NSEntityDescription *)findOneByCriteria:(NSString *)criteria withValue:(id)value;
-(NSEntityDescription *)findOne:(NSString *)entity ByCriteria:(NSString *)criteria withValue:(id)value;
-(NSArray *)findAll;

@end
