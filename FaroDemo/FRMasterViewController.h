//
//  FRMasterViewController.h
//  FaroDemo
//
//  Created by Jorge Leonardo Monge García on 22/06/14.
//  Copyright (c) 2014 Jorge Leonardo Monge García. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <CoreData/CoreData.h>

@interface FRMasterViewController : UITableViewController <NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end
