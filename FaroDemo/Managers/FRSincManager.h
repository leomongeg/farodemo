//
//  FRSincManager.h
//  FaroDemo
//
//  Created by Jorge Leonardo Monge García on 15/08/14.
//  Copyright (c) 2014 Jorge Leonardo Monge García. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FRSincManager : NSObject


@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@property (nonatomic, strong) void (^callbackBlock)(Boolean result); // Use copy if not using ARC

- (id)initWithManagedObjectContext:(NSManagedObjectContext *) managedObjectContext andBlock:(void (^)(Boolean result))callbackBlock;
- (id)initWithManagedObjectContext:(NSManagedObjectContext *) managedObjectContext;
- (void)didFinishedServerSinc:(NSNotification *)notification;
-(void)sincData;

@end
