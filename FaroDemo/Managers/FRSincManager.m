//
//  FRSincManager.m
//  FaroDemo
//
//  Created by Jorge Leonardo Monge García on 15/08/14.
//  Copyright (c) 2014 Jorge Leonardo Monge García. All rights reserved.
//

#import "FRSincManager.h"
#import "FRStoreRepository.h"
#import "FRBeaconRepository.h"
#import "FRPromoRepository.h"

@implementation FRSincManager
@synthesize managedObjectContext = _managedObjectContext;
@synthesize callbackBlock        = _callbackBlock;


- (id)initWithManagedObjectContext:(NSManagedObjectContext *) managedObjectContext andBlock:(void (^)(Boolean result))callbackBlock
{
    self = [super init];
    
    if(self)
    {
        _managedObjectContext = managedObjectContext;
        _callbackBlock        = callbackBlock;
    }
    
    return self;
}

- (id)initWithManagedObjectContext:(NSManagedObjectContext *) managedObjectContext
{
    self = [super init];
    
    if(self)
    {
        _managedObjectContext = managedObjectContext;
    }
    
    return self;
}


-(void)sincData
{
    
//    [NSThread sleepForTimeInterval:10];
//    FRStoreRepository *storeRepo = [[FRStoreRepository alloc] initWithManagedObjectContext:_managedObjectContext];
//    
////    [storeRepo syncStoresFromServer];
//    
//    FRBeaconRepository *beaconRepo = [[FRBeaconRepository alloc] initWithManagedObjectContext:_managedObjectContext];
//    
//    [beaconRepo syncBeaconsFromServer];
//    
//    FRPromoRepository *promoRepo = [[FRPromoRepository alloc] initWithManagedObjectContext:_managedObjectContext];
//    
////    [promoRepo syncPromosFromServer];
//    
//    _callbackBlock(true);
//    return;
    
    NSManagedObjectContext *context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [context setPersistentStoreCoordinator:_managedObjectContext.persistentStoreCoordinator];
    [context setUndoManager:nil];
    [context performBlock:^{
        // Do lots of things with the context.
        @autoreleasepool {
        FRStoreRepository *storeRepo = [[FRStoreRepository alloc] initWithManagedObjectContext:context];
        
        [storeRepo syncStoresFromServer];
        
        FRBeaconRepository *beaconRepo = [[FRBeaconRepository alloc] initWithManagedObjectContext:context];
        
        [beaconRepo syncBeaconsFromServer];
        
        FRPromoRepository *promoRepo = [[FRPromoRepository alloc] initWithManagedObjectContext:context];
        
        [promoRepo syncPromosFromServer];
        
        
        NSLog(@"Listo!");
            };
        _callbackBlock(true);
        
    }];
    context = nil;
    
//    for (NSManagedObject *mo in [context registeredObjects]) {
//        [context refreshObject:mo mergeChanges:NO];
//    }
    
}

-(void)didFinishedServerSinc:(NSNotification *)notification
{
    
}

-(void)dealloc
{
    NSLog(@"DEALLOC SINC MANAGER");
    _managedObjectContext = nil;
    _callbackBlock        = nil;
}

@end
