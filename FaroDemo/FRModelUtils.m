//
//  FRModelUtils.m
//  FaroDemo
//
//  Created by Jorge Leonardo Monge García on 03/08/14.
//  Copyright (c) 2014 Jorge Leonardo Monge García. All rights reserved.
//

#import "FRModelUtils.h"

@implementation FRModelUtils

+(NSEntityDescription *)findOne:(NSString *)entity ByCriteria:(NSString *)criteria withValue:(id)value inMOC:(NSManagedObjectContext *) managedObjectContext
{
    if(value == [NSNull null])
        return nil;
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription
                        entityForName:entity inManagedObjectContext:managedObjectContext]];
    [request setFetchBatchSize:1];
    [request setFetchLimit:1];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"%@ = %@", criteria, value]];
    [request setPredicate:predicate];
    
    NSError *sqlError;
    NSArray *objetcs = [managedObjectContext executeFetchRequest:request error:&sqlError];
    request          = nil;
    predicate        = nil;
    
    if(sqlError)
    {
        NSLog(@"%@", sqlError);
        return nil;
    }
    
    if([objetcs count] > 0)
        return [objetcs objectAtIndex:0];
    
    return nil;
}

@end
