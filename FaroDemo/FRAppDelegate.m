//
//  FRAppDelegate.m
//  FaroDemo
//
//  Created by Jorge Leonardo Monge García on 22/06/14.
//  Copyright (c) 2014 Jorge Leonardo Monge García. All rights reserved.
//

#import "FRAppDelegate.h"

#import "FRMasterViewController.h"
#import "ESTBeaconManager.h"
#import "FRRegionsManager.h"
#import "FRBeaconRepository.h"
#import "Beacon.h"
#import "Store.h"

#import "FRSincManager.h"

@interface FRAppDelegate () <CLLocationManagerDelegate>

@property (nonatomic, strong) ESTBeaconManager *beaconManager;
@property (strong, nonatomic) CLLocationManager *locationManager;

@property (strong, nonatomic) FRRegionsManager *regionsManager;

@end


@implementation FRAppDelegate

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize beaconManager = _beaconManager;
@synthesize regionsManager = _regionsManager;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    
    _regionsManager = [[FRRegionsManager alloc] initWithManagedObjectContext:[self managedObjectContext]];
    
    FRSincManager *sync = [[FRSincManager alloc] initWithManagedObjectContext:[self managedObjectContext] andBlock:^(Boolean result){
        
        if(result)
        {
            [_regionsManager performSelectorOnMainThread:@selector(initBeaconManager) withObject:nil waitUntilDone:YES];
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [_regionsManager initBeaconManager];
//                    });
//            [_regionsManager initBeaconManager];
        }
    }];
    
    [sync sincData];
    sync = nil;
    
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    // Override point for customization after application launch.
//    UINavigationController *navigationController = (UINavigationController *)self.window.rootViewController;
//    FRMasterViewController *controller = (FRMasterViewController *)navigationController.topViewController;
//    controller.managedObjectContext = self.managedObjectContext;
    return YES;
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"ERROR %@",error);
}

-(void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region
{
    NSLog(@"Entro en la region");
    if ([region isKindOfClass:[CLBeaconRegion class]]) {
        
        
        FRBeaconRepository *repo = [[FRBeaconRepository alloc]initWithManagedObjectContext:[self managedObjectContext]];
        
        NSString *serverIdStr = [region.identifier stringByReplacingOccurrencesOfString:@"beacon_region_" withString:@""];
        
        Beacon *beacon = (Beacon *)[repo findOneByCriteria:@"serverId" withValue:[NSNumber numberWithInteger:[serverIdStr integerValue]]];
        
        NSLog(@"%@", beacon.store.name);
        
        UILocalNotification *notification = [[UILocalNotification alloc] init];
//        notification.alertBody = [NSString stringWithFormat:@"Entro en la recion con CLLocationManager %@", region.identifier];
        notification.alertBody = [NSString stringWithFormat:@"Entro en la recion con CLLocationManager %@", beacon.store.name];
        notification.soundName = @"Default";
        [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
        NSUserDefaults *myUserDefaults = [NSUserDefaults standardUserDefaults];
        [myUserDefaults setObject: [NSString stringWithFormat:@"Entro en la recion con CLLocationManager %@", region.identifier] forKey: @"ultimaNot"];
        //Almacenar en el NSUserDefaults el identificador (minor y major) del beacon y un timestamp para no mostrar la notificacion en cada evento.
    }
}

#pragma mark - ESTBeaconManager delegate

-(void) beaconManager:(ESTBeaconManager *)manager didExitRegion:(ESTBeaconRegion *)region
{
    NSLog(@"Salio de la region");
    NSUserDefaults *myUserDefaults = [NSUserDefaults standardUserDefaults];
    [myUserDefaults setObject: @"the key" forKey: @"key"];
    UILocalNotification *notification = [[UILocalNotification alloc] init];
//    notification.alertBody = [NSString stringWithFormat:@"Salio de la region %@", region re];
    notification.soundName = UILocalNotificationDefaultSoundName;
    
    [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
}

//-(void) beaconManager:(ESTBeaconManager *)manager didEnterRegion:(ESTBeaconRegion *)region
//{
//    NSLog(@"Entro en la region");
//}
//
//-(void)beaconManager:(ESTBeaconManager *)manager didStartMonitoringForRegion:(CLRegion *)region
//{
//    NSLog(@"Entro");
//}

//- (void)beaconManager:(ESTBeaconManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(ESTBeaconRegion *)region
//{
//    NSLog(@"Beacons %d", [beacons count]);
//    if (beacons.count > 0)
//    {
//        ESTBeacon *firstBeacon = [beacons firstObject];
//        NSLog(@"Region: %@", region.identifier);
//        NSLog(@"Becon major: %@", firstBeacon.major);
//        NSLog(@"Becon minor: %@", firstBeacon.minor);
//        NSLog(@"Becon proximidad: %@", [self textForProximity:firstBeacon.proximity]);
//        
//        
//        //        self.zoneLabel.text     = [self textForProximity:firstBeacon.proximity];
//        //        self.imageView.image    = [self imageForProximity:firstBeacon.proximity];
//    }
//}

- (NSString *)textForProximity:(CLProximity)proximity
{
    switch (proximity) {
        case CLProximityFar:
            return @"Far";
            break;
        case CLProximityNear:
            return @"Near";
            break;
        case CLProximityImmediate:
            return @"Immediate";
            break;
            
        default:
            return @"Unknown";
            break;
    }
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
             // Replace this implementation with code to handle the error appropriately.
             // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        } 
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"FaroDemo" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"FaroDemo.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }    
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end
