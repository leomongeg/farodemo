//
//  StoreCategory.m
//  FaroDemo
//
//  Created by Jorge Leonardo Monge García on 03/08/14.
//  Copyright (c) 2014 Jorge Leonardo Monge García. All rights reserved.
//

#import "StoreCategory.h"
#import "Store.h"


@implementation StoreCategory

@dynamic timeStamp;
@dynamic name;
@dynamic descriptionStr;
@dynamic image;
@dynamic imageUrl;
@dynamic updated_at_utc;
@dynamic serverId;
@dynamic stores;

@end
