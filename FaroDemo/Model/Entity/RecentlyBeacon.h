//
//  RecentlyBeacon.h
//  FaroDemo
//
//  Created by Jorge Leonardo Monge García on 12/08/14.
//  Copyright (c) 2014 Jorge Leonardo Monge García. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface RecentlyBeacon : NSManagedObject

@property (nonatomic, retain) NSNumber * major;
@property (nonatomic, retain) NSNumber * minor;
@property (nonatomic, retain) NSString * uuid;
@property (nonatomic, retain) NSString * key;
@property (nonatomic, retain) NSNumber * serverId;
@property (nonatomic, retain) NSNumber * timestamp;

@end
