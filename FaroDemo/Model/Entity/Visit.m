//
//  Visit.m
//  FaroDemo
//
//  Created by Jorge Leonardo Monge García on 03/08/14.
//  Copyright (c) 2014 Jorge Leonardo Monge García. All rights reserved.
//

#import "Visit.h"
#import "Beacon.h"
#import "Store.h"


@implementation Visit

@dynamic beaconServerId;
@dynamic localServerId;
@dynamic created_at_utc;
@dynamic beacon;
@dynamic store;

@end
