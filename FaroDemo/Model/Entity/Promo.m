//
//  Promo.m
//  FaroDemo
//
//  Created by Jorge Leonardo Monge García on 03/08/14.
//  Copyright (c) 2014 Jorge Leonardo Monge García. All rights reserved.
//

#import "Promo.h"
#import "Beacon.h"
#import "Store.h"


@implementation Promo

@dynamic timeStamp;
@dynamic title;
@dynamic descriptionStr;
@dynamic active;
@dynamic startDate;
@dynamic endDate;
@dynamic image1;
@dynamic image1Url;
@dynamic image2;
@dynamic image2Url;
@dynamic updated_at_utc;
@dynamic serverId;
@dynamic type;
@dynamic store;
@dynamic beacon;

@end
