//
//  Beacon.h
//  FaroDemo
//
//  Created by Jorge Leonardo Monge García on 03/08/14.
//  Copyright (c) 2014 Jorge Leonardo Monge García. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Store;

@interface Beacon : NSManagedObject

@property (nonatomic, retain) NSDate * timeStamp;
@property (nonatomic, retain) NSNumber * serverId;
@property (nonatomic, retain) NSString * uuid;
@property (nonatomic, retain) NSNumber * major;
@property (nonatomic, retain) NSNumber * minor;
@property (nonatomic, retain) NSString * serial;
@property (nonatomic, retain) NSString * color;
@property (nonatomic, retain) NSNumber * storeId;
@property (nonatomic, retain) NSDate * updated_at_utc;
@property (nonatomic, retain) Store *store;
@property (nonatomic, retain) NSSet *promos;
@property (nonatomic, retain) NSSet *visits;
@end

@interface Beacon (CoreDataGeneratedAccessors)

- (void)addPromosObject:(NSManagedObject *)value;
- (void)removePromosObject:(NSManagedObject *)value;
- (void)addPromos:(NSSet *)values;
- (void)removePromos:(NSSet *)values;

- (void)addVisitsObject:(NSManagedObject *)value;
- (void)removeVisitsObject:(NSManagedObject *)value;
- (void)addVisits:(NSSet *)values;
- (void)removeVisits:(NSSet *)values;

@end
