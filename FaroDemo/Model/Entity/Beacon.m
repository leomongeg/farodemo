//
//  Beacon.m
//  FaroDemo
//
//  Created by Jorge Leonardo Monge García on 03/08/14.
//  Copyright (c) 2014 Jorge Leonardo Monge García. All rights reserved.
//

#import "Beacon.h"
#import "Store.h"


@implementation Beacon

@dynamic timeStamp;
@dynamic serverId;
@dynamic uuid;
@dynamic major;
@dynamic minor;
@dynamic serial;
@dynamic color;
@dynamic storeId;
@dynamic updated_at_utc;
@dynamic store;
@dynamic promos;
@dynamic visits;

@end
