//
//  Store.h
//  FaroDemo
//
//  Created by Jorge Leonardo Monge García on 03/08/14.
//  Copyright (c) 2014 Jorge Leonardo Monge García. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Store : NSManagedObject

@property (nonatomic, retain) NSDate * timeStamp;
@property (nonatomic, retain) NSString * uid;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * location;
@property (nonatomic, retain) NSString * mail;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSNumber * active;
@property (nonatomic, retain) NSString * descriptionStr;
@property (nonatomic, retain) NSString * schedule;
@property (nonatomic, retain) NSDate * updated_at_utc;
@property (nonatomic, retain) NSString * image1;
@property (nonatomic, retain) NSString * image1Url;
@property (nonatomic, retain) NSString * image2;
@property (nonatomic, retain) NSString * image2Url;
@property (nonatomic, retain) NSString * image3;
@property (nonatomic, retain) NSString * image3Url;
@property (nonatomic, retain) NSNumber * serverId;
@property (nonatomic, retain) NSManagedObject *category;
@property (nonatomic, retain) NSSet *beacons;
@property (nonatomic, retain) NSSet *promos;
@property (nonatomic, retain) NSSet *visits;
@end

@interface Store (CoreDataGeneratedAccessors)

- (void)addBeaconsObject:(NSManagedObject *)value;
- (void)removeBeaconsObject:(NSManagedObject *)value;
- (void)addBeacons:(NSSet *)values;
- (void)removeBeacons:(NSSet *)values;

- (void)addPromosObject:(NSManagedObject *)value;
- (void)removePromosObject:(NSManagedObject *)value;
- (void)addPromos:(NSSet *)values;
- (void)removePromos:(NSSet *)values;

- (void)addVisitsObject:(NSManagedObject *)value;
- (void)removeVisitsObject:(NSManagedObject *)value;
- (void)addVisits:(NSSet *)values;
- (void)removeVisits:(NSSet *)values;

@end
