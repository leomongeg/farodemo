//
//  Visit.h
//  FaroDemo
//
//  Created by Jorge Leonardo Monge García on 03/08/14.
//  Copyright (c) 2014 Jorge Leonardo Monge García. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Beacon, Store;

@interface Visit : NSManagedObject

@property (nonatomic, retain) NSNumber * beaconServerId;
@property (nonatomic, retain) NSNumber * localServerId;
@property (nonatomic, retain) NSDate * created_at_utc;
@property (nonatomic, retain) Beacon *beacon;
@property (nonatomic, retain) Store *store;

@end
