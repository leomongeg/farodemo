//
//  Promo.h
//  FaroDemo
//
//  Created by Jorge Leonardo Monge García on 03/08/14.
//  Copyright (c) 2014 Jorge Leonardo Monge García. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Beacon, Store;

@interface Promo : NSManagedObject

@property (nonatomic, retain) NSDate * timeStamp;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * descriptionStr;
@property (nonatomic, retain) NSNumber * active;
@property (nonatomic, retain) NSDate * startDate;
@property (nonatomic, retain) NSDate * endDate;
@property (nonatomic, retain) NSString * image1;
@property (nonatomic, retain) NSString * image1Url;
@property (nonatomic, retain) NSString * image2;
@property (nonatomic, retain) NSString * image2Url;
@property (nonatomic, retain) NSDate * updated_at_utc;
@property (nonatomic, retain) NSNumber * serverId;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) Store *store;
@property (nonatomic, retain) Beacon *beacon;

@end
