//
//  StoreCategory.h
//  FaroDemo
//
//  Created by Jorge Leonardo Monge García on 03/08/14.
//  Copyright (c) 2014 Jorge Leonardo Monge García. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Store;

@interface StoreCategory : NSManagedObject

@property (nonatomic, retain) NSDate * timeStamp;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * descriptionStr;
@property (nonatomic, retain) NSString * image;
@property (nonatomic, retain) NSString * imageUrl;
@property (nonatomic, retain) NSDate * updated_at_utc;
@property (nonatomic, retain) NSNumber * serverId;
@property (nonatomic, retain) NSSet *stores;
@end

@interface StoreCategory (CoreDataGeneratedAccessors)

- (void)addStoresObject:(Store *)value;
- (void)removeStoresObject:(Store *)value;
- (void)addStores:(NSSet *)values;
- (void)removeStores:(NSSet *)values;

@end
