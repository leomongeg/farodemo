//
//  Store.m
//  FaroDemo
//
//  Created by Jorge Leonardo Monge García on 03/08/14.
//  Copyright (c) 2014 Jorge Leonardo Monge García. All rights reserved.
//

#import "Store.h"


@implementation Store

@dynamic timeStamp;
@dynamic uid;
@dynamic name;
@dynamic location;
@dynamic mail;
@dynamic phone;
@dynamic url;
@dynamic active;
@dynamic descriptionStr;
@dynamic schedule;
@dynamic updated_at_utc;
@dynamic image1;
@dynamic image1Url;
@dynamic image2;
@dynamic image2Url;
@dynamic image3;
@dynamic image3Url;
@dynamic serverId;
@dynamic category;
@dynamic beacons;
@dynamic promos;
@dynamic visits;

@end
