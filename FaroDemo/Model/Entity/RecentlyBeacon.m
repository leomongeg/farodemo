//
//  RecentlyBeacon.m
//  FaroDemo
//
//  Created by Jorge Leonardo Monge García on 12/08/14.
//  Copyright (c) 2014 Jorge Leonardo Monge García. All rights reserved.
//

#import "RecentlyBeacon.h"


@implementation RecentlyBeacon

@dynamic major;
@dynamic minor;
@dynamic uuid;
@dynamic key;
@dynamic serverId;
@dynamic timestamp;

@end
