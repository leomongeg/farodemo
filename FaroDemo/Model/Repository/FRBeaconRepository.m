//
//  FRBeaconReposotory.m
//  FaroDemo
//
//  Created by Jorge Leonardo Monge García on 04/08/14.
//  Copyright (c) 2014 Jorge Leonardo Monge García. All rights reserved.
//

#import "FRBeaconRepository.h"
#import "FRFaro.h"
#import "Beacon.h"
#import "Store.h"
#import "FRStoreRepository.h"

@implementation FRBeaconRepository

-(void)syncBeaconsFromServer
{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"beacons" ofType:@"json"];
    NSError *error = nil;
    NSData *JSONData = [NSData dataWithContentsOfFile:filePath options:NSDataReadingMappedIfSafe error:&error];
    NSArray *beacons;
    if (JSONData)
    {
        beacons = [NSJSONSerialization JSONObjectWithData:JSONData options:0 error:&error];
        [self processSincJSONData:beacons];
    }
    
    NSLog(@"Json string: %@ ", beacons);
}

-(void)processSincJSONData:(NSArray *)beacons
{
    FRStoreRepository *storeRepo = [[FRStoreRepository alloc] initWithManagedObjectContext:self.managedObjectContext];
    NSDictionary *beaconDict = [beacons objectAtIndex:0];
    int i = 0;
//    while (i < 10000)
    for (NSDictionary *beaconDict in beacons)
    {
        i ++;
        Beacon *beacon = [self getBeaconFromDict:beaconDict andI:(int)i];
        Store *store   = (Store *)[storeRepo findOneByCriteria:@"serverId" withValue:[NSNumber numberWithInteger:[[beaconDict objectForKey:@"local_id"] integerValue]]];
        
        if(store)
        {
            [beacon setStore:store];
            [beacon setStoreId:store.serverId];
        }
        NSError *contextError;
        if(![self.managedObjectContext save:&contextError])
        {
            //Manejar correctamente el error...
            NSLog(@" Error al Guardar Beacons--> %@, /n %@, /n %@", contextError, [contextError userInfo], [contextError localizedDescription]);
        }
    }
    
    
}

-(Beacon *)getBeaconFromDict:(NSDictionary *)beaconDict andI:(int)i
{
    if(beaconDict == nil)
        return nil;
    
    NSInteger serverId = [[beaconDict objectForKey:@"id"] integerValue];
//    NSInteger serverId =  [[NSNumber numberWithInt:i] integerValue];
    Beacon *beacon = (Beacon *)[self findOneByCriteria:@"serverId" withValue:[NSNumber numberWithInteger:serverId]];
    
    if (beacon == nil)
        beacon = [NSEntityDescription
                  insertNewObjectForEntityForName:@"Beacon" inManagedObjectContext:self.managedObjectContext];
    
    [beacon setServerId:[NSNumber numberWithInteger:serverId]];
    [beacon setUuid:[beaconDict objectForKey:@"uid"]];
    [beacon setMajor:[beaconDict objectForKey:@"maximo"]];
    [beacon setMinor:[beaconDict objectForKey:@"minimo"]];
    [beacon setColor:[beaconDict objectForKey:@"color"]];
    [beacon setSerial:[beaconDict objectForKey:@"serie"]];
    [beacon setUpdated_at_utc:[FRFaro getUTCFormateDateFromString:[beaconDict objectForKey:@"updated_at_utc"]]];
    
    return beacon;
}

@end
