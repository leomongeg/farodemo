//
//  FRStoreRepository.m
//  FaroDemo
//
//  Created by Jorge Leonardo Monge García on 03/08/14.
//  Copyright (c) 2014 Jorge Leonardo Monge García. All rights reserved.
//

#import "FRStoreRepository.h"
#import "WebServiceConnect.h"
#import "Store.h"
#import "StoreCategory.h"
#import "FRFaro.h"
#import "FRModelUtils.h"

@implementation FRStoreRepository


-(void)processSincJSONData:(NSArray *)storesData
{
    for (NSDictionary *storeDict in storesData)
    {
        NSDictionary *dicCategory = [storeDict objectForKey:@"categoria"];
        StoreCategory *category = [self getCategoryFromDic:dicCategory];
        Store *store = [self getStoreFromDict:storeDict];
        if (store && category)
            [store setCategory:category];
//        [self findOneByCriteria:@"serverId" withValue:[NSNumber numberWithInt:1]];
        NSLog(@"Store: %@", store);
        NSError *contextError;
        if(![self.managedObjectContext save:&contextError])
        {
            //Manejar correctamente el error...
            NSLog(@" Error al Guardar Store y Categoria--> %@, /n %@, /n %@", contextError, [contextError userInfo], [contextError localizedDescription]);
        }
    }
    NSError *contextError;

    if(![self.managedObjectContext save:&contextError])
    {
        //Manejar correctamente el error...
        NSLog(@" Error al Guardar Store y Categoria--> %@, /n %@, /n %@", contextError, [contextError userInfo], [contextError localizedDescription]);
    }
    
    
}

-(void)syncStoresFromServer
{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"stores" ofType:@"json"];
    NSError *error = nil;
    NSData *JSONData = [NSData dataWithContentsOfFile:filePath options:NSDataReadingMappedIfSafe error:&error];
    NSArray *stores;
    if (JSONData)
    {
        stores = [NSJSONSerialization JSONObjectWithData:JSONData options:0 error:&error];
        [self processSincJSONData:stores];
    }
    
    NSLog(@"Json string: %@ ", stores);
}

-(Store *)getStoreFromDict:(NSDictionary *)dictStore
{
    if(dictStore == nil)
        return nil;
    
    NSInteger serverId = [[dictStore objectForKey:@"id"] integerValue];
    Store *store       = (Store *)[self findOneByCriteria:@"serverId" withValue:[NSNumber numberWithInteger:serverId]];
    
    if (store == nil)
        store = [NSEntityDescription
             insertNewObjectForEntityForName:@"Store" inManagedObjectContext:self.managedObjectContext];
    
    [store setServerId:[NSNumber numberWithInteger:serverId]];
    [store setUid:[dictStore objectForKey:@"uid"]];
    [store setName:[dictStore objectForKey:@"nombre"]];
    [store setDescriptionStr:[dictStore objectForKey:@"descripcion"]];
    [store setActive:[NSNumber numberWithBool:[[dictStore objectForKey:@"activo"] boolValue]]];
    [store setLocation:[dictStore objectForKey:@"ubicacion"]];
    [store setUrl:[dictStore objectForKey:@"url"]];
    [store setSchedule:[dictStore objectForKey:@"horario"]];
    [store setImage1:[dictStore objectForKey:@"imagen1"]];
    [store setImage1Url:[dictStore objectForKey:@"imagen1Url"]];
    [store setImage2:[dictStore objectForKey:@"imagen2"]];
    [store setImage2Url:[dictStore objectForKey:@"imagen2Url"]];
    [store setImage3:[dictStore objectForKey:@"imagen3"]];
    [store setImage3Url:[dictStore objectForKey:@"imagen3Url"]];
    [store setUpdated_at_utc:[FRFaro getUTCFormateDateFromString:[dictStore objectForKey:@"updated_at_utc"]]];
    
    return store;
}

-(StoreCategory *)getCategoryFromDic:(NSDictionary *)dicCategory
{
    if(dicCategory == nil)
        return nil;
    
    NSInteger serverId      = [[dicCategory objectForKey:@"id"] integerValue];
    StoreCategory *category = (StoreCategory *) [self findOne:@"StoreCategory" ByCriteria:@"serverId" withValue:[NSNumber numberWithInteger:serverId]];
    
    if(category == nil)
        category = [NSEntityDescription
                insertNewObjectForEntityForName:@"StoreCategory" inManagedObjectContext:self.managedObjectContext];
    
    [category setServerId:[NSNumber numberWithInteger:serverId]];
    [category setName:[dicCategory objectForKey:@"nombre"]];
    [category setDescriptionStr:[dicCategory objectForKey:@"descripcion"]];
    [category setImage:[dicCategory objectForKey:@"imagen"]];
    [category setImageUrl:[dicCategory objectForKey:@"imagenUrl"]];
    [category setUpdated_at_utc:[FRFaro getUTCFormateDateFromString:[dicCategory objectForKey:@"updated_at_utc"]]];
    

    return category;
}

@end
