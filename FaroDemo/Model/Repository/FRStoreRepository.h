//
//  FRStoreRepository.h
//  FaroDemo
//
//  Created by Jorge Leonardo Monge García on 03/08/14.
//  Copyright (c) 2014 Jorge Leonardo Monge García. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FRBaseRepository.h"

@interface FRStoreRepository : FRBaseRepository

-(void)processSincJSONData:(NSArray *)storesData;
-(void)syncStoresFromServer;

@end
