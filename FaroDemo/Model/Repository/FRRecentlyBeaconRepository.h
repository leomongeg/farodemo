//
//  FRRecentlyBeaconRepository.h
//  FaroDemo
//
//  Created by Jorge Leonardo Monge García on 12/08/14.
//  Copyright (c) 2014 Jorge Leonardo Monge García. All rights reserved.
//

#import "FRBaseRepository.h"
#import "ESTBeacon.h"

@interface FRRecentlyBeaconRepository : FRBaseRepository

-(Boolean) hasRecentlyActivity:(NSInteger)serverId;
-(void)registerActivity:(ESTBeacon *)beacon andServerId:(NSInteger)serverId;
-(Boolean)verifyActivity:(ESTBeacon *)beacon andServerId:(NSInteger)serverId andKey:(NSString *)key;
-(void)deleteAllActivity;

@end
