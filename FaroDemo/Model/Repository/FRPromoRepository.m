//
//  FRPromoRepository.m
//  FaroDemo
//
//  Created by Jorge Leonardo Monge García on 10/08/14.
//  Copyright (c) 2014 Jorge Leonardo Monge García. All rights reserved.
//

#import "FRPromoRepository.h"
#import "FRBeaconRepository.h"
#import "Beacon.h"

@implementation FRPromoRepository

-(void)syncPromosFromServer
{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"promos" ofType:@"json"];
    NSError *error = nil;
    NSData *JSONData = [NSData dataWithContentsOfFile:filePath options:NSDataReadingMappedIfSafe error:&error];
    NSArray *promos;
    if (JSONData)
    {
        promos = [NSJSONSerialization JSONObjectWithData:JSONData options:0 error:&error];
        [self processSincJSONData:promos];
    }
    
    NSLog(@"Json string: %@ ", promos);
}

-(void)processSincJSONData:(NSArray *)promos
{
    FRBeaconRepository *beaconRepo = [[FRBeaconRepository alloc]initWithManagedObjectContext:self.managedObjectContext];
    
    for (NSDictionary *promoDict in promos)
    {
        Promo *promo = [self getPromoFromDict:(NSDictionary *)promoDict];
        Beacon *beacon   = (Beacon *)[beaconRepo findOneByCriteria:@"serverId" withValue:[NSNumber numberWithInteger:[[promoDict objectForKey:@"beacon_id"] integerValue]]];
        
        
        if(promo)
        {
            [promo setBeacon:beacon];
            [promo setStore:beacon.store];
        }
        
    }
    
    NSError *contextError;
    if(![self.managedObjectContext save:&contextError])
    {
        //Manejar correctamente el error...
        NSLog(@" Error al Guardar Promociones--> %@, /n %@, /n %@", contextError, [contextError userInfo], [contextError localizedDescription]);
    }
}

-(Promo *)getPromoFromDict:(NSDictionary *)promoDict
{
    if(promoDict == nil)
        return nil;
    
    NSInteger serverId = [[promoDict objectForKey:@"id"] integerValue];
    
    Promo *promo = (Promo *)[self findOneByCriteria:@"serverId" withValue:[NSNumber numberWithInteger:serverId]];
    
    if (promo == nil)
        promo = [NSEntityDescription
                  insertNewObjectForEntityForName:@"Promo" inManagedObjectContext:self.managedObjectContext];
    
    [promo setServerId:[NSNumber numberWithInteger:serverId]];
    [promo setTitle:[promoDict objectForKey:@"titulo"]];
    [promo setDescriptionStr:[promoDict objectForKey:@"texto"]];
    [promo setType:[promoDict objectForKey:@"type"]];
    [promo setImage1:[promoDict objectForKey:@"imagen1"]];
    
    return promo;

}

-(Promo *)getPromoByBeaconMinor:(NSInteger) minor withMajor:(NSInteger)major andDistance:(NSString *)distance
{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription
                        entityForName:@"Promo" inManagedObjectContext:self.managedObjectContext]];
    [request setFetchBatchSize:1];
    [request setFetchLimit:1];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"beacon.minor = %ld AND beacon.major = %ld AND type == [c] \"%@\"",minor, major, distance]];
    
    [request setPredicate:predicate];
    
    NSError *sqlError;
    NSArray *objetcs = [self.managedObjectContext executeFetchRequest:request error:&sqlError];
    request          = nil;
    predicate        = nil;
    
    if(sqlError)
    {
        NSLog(@"%@", sqlError);
        return nil;
    }
    
    if([objetcs count] > 0)
        return [objetcs objectAtIndex:0];
    
    return nil;
}

@end
