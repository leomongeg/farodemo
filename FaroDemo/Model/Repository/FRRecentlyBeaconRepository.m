//
//  FRRecentlyBeaconRepository.m
//  FaroDemo
//
//  Created by Jorge Leonardo Monge García on 12/08/14.
//  Copyright (c) 2014 Jorge Leonardo Monge García. All rights reserved.
//

#import "FRRecentlyBeaconRepository.h"
#import "RecentlyBeacon.h"

@implementation FRRecentlyBeaconRepository

-(Boolean) hasRecentlyActivity:(NSInteger)serverId
{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription
                        entityForName:@"RecentlyBeacon" inManagedObjectContext:self.managedObjectContext]];
    [request setFetchBatchSize:1];
    [request setFetchLimit:1];
    
    NSTimeInterval ti = [[NSDate date] timeIntervalSince1970];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"(timestamp - %f) > %d AND serverId = %ld", ti, 1, serverId]];
    
    
    [request setPredicate:predicate];
    
    NSError *sqlError;
    NSArray *objetcs = [self.managedObjectContext executeFetchRequest:request error:&sqlError];
    request          = nil;
    predicate        = nil;
    
    if(sqlError)
    {
        NSLog(@"%@", sqlError);
        return false;
    }
    
    if([objetcs count] > 0)
        return true;
    
    return false;
}

-(void)registerActivity:(ESTBeacon *)beacon andServerId:(NSInteger)serverId
{
    RecentlyBeacon *activity = [NSEntityDescription
              insertNewObjectForEntityForName:@"RecentlyBeacon" inManagedObjectContext:self.managedObjectContext];
    
    [activity setServerId:[NSNumber numberWithInteger:serverId]];
    [activity setUuid:[NSString stringWithFormat:@"%@", beacon.proximityUUID]];
    [activity setMajor:beacon.major];
    [activity setMinor:beacon.minor];
    [activity setKey:[NSString stringWithFormat:@"beacon_region_%ld", serverId]];
    [activity setTimestamp:[NSNumber numberWithFloat:[[NSDate date] timeIntervalSince1970]]];
    
    NSError *contextError;
    if(![self.managedObjectContext save:&contextError])
    {
        //Manejar correctamente el error...
        NSLog(@" Error al Guardar Actividad reciente beacon --> %@, /n %@, /n %@", contextError, [contextError userInfo], [contextError localizedDescription]);
    }
    
}

//Has recently activity:
-(Boolean)verifyActivity:(ESTBeacon *)beacon andServerId:(NSInteger)serverId andKey:(NSString *)key
{
    Boolean result           = false;
    RecentlyBeacon *activity = (RecentlyBeacon *)[self recentlyActivityByKey:key];
    NSTimeInterval timestamp = [[NSDate date] timeIntervalSince1970];
    
    if(activity == nil)
    {
        activity = [NSEntityDescription
                                insertNewObjectForEntityForName:@"RecentlyBeacon" inManagedObjectContext:self.managedObjectContext];
        [activity setServerId:[NSNumber numberWithInteger:serverId]];
        [activity setUuid:[NSString stringWithFormat:@"%@", beacon.proximityUUID]];
        [activity setMajor:beacon.major];
        [activity setMinor:beacon.minor];
        [activity setKey:key
         ];
        [activity setTimestamp:[NSNumber numberWithDouble:timestamp]];
        result = true;
    }
    
    if(timestamp - [activity.timestamp doubleValue] > 60)
    {
        [activity setTimestamp:[NSNumber numberWithDouble:timestamp]];
        result = true;
    }
    
    
    if(result)
    {
        NSError *contextError;
        if(![self.managedObjectContext save:&contextError])
        {
            //Manejar correctamente el error...
            NSLog(@" Error al Guardar Actividad reciente beacon --> %@, /n %@, /n %@", contextError, [contextError userInfo], [contextError localizedDescription]);
        }
    }
    
    activity = nil;
    
    return result;
}

-(RecentlyBeacon *)recentlyActivityByKey:(NSString *)key
{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription
                        entityForName:@"RecentlyBeacon" inManagedObjectContext:self.managedObjectContext]];
    [request setFetchBatchSize:1];
    [request setFetchLimit:1];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"key == [c] \"%@\"", key]];
    [request setPredicate:predicate];
    
    NSError *sqlError;
    NSArray *objetcs = [self.managedObjectContext executeFetchRequest:request error:&sqlError];
    request          = nil;
    predicate        = nil;
    
    if(sqlError)
    {
        NSLog(@"%@", sqlError);
        return nil;
    }
    
    if([objetcs count] > 0)
        return [objetcs objectAtIndex:0];
    
    return nil;
}


-(void)deleteAllActivity
{
    NSFetchRequest * request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:@"RecentlyBeacon" inManagedObjectContext:self.managedObjectContext]];
    [request setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error       = nil;
    NSArray * activities  = [self.managedObjectContext executeFetchRequest:request error:&error];
    request               = nil;
    //error handling goes here
    for (NSManagedObject * activity in activities)
    {
        [self.managedObjectContext deleteObject:activity];
    }
    
    NSError *saveError = nil;
    [self.managedObjectContext save:&saveError];
}

/*
-(RecentlyBeacon *)getReciente:(NSInteger)serverId
{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription
                        entityForName:@"RecentlyBeacon" inManagedObjectContext:self.managedObjectContext]];
    [request setFetchBatchSize:1];
    [request setFetchLimit:1];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"%@ = %@", criteria, value]];
    [request setPredicate:predicate];
    
    NSError *sqlError;
    NSArray *objetcs = [_managedObjectContext executeFetchRequest:request error:&sqlError];
    request          = nil;
    predicate        = nil;
    
    if(sqlError)
    {
        NSLog(@"%@", sqlError);
        return nil;
    }
    
    if([objetcs count] > 0)
        return [objetcs objectAtIndex:0];
    
    return nil;
}
*/
@end
