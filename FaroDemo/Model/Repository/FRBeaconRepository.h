//
//  FRBeaconReposotory.h
//  FaroDemo
//
//  Created by Jorge Leonardo Monge García on 04/08/14.
//  Copyright (c) 2014 Jorge Leonardo Monge García. All rights reserved.
//

#import "FRBaseRepository.h"

@interface FRBeaconRepository : FRBaseRepository

-(void)syncBeaconsFromServer;

@end
