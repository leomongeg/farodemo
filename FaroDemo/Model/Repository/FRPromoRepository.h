//
//  FRPromoRepository.h
//  FaroDemo
//
//  Created by Jorge Leonardo Monge García on 10/08/14.
//  Copyright (c) 2014 Jorge Leonardo Monge García. All rights reserved.
//

#import "FRBaseRepository.h"
#import "Promo.h"

@interface FRPromoRepository : FRBaseRepository

- (void)syncPromosFromServer;
- (Promo *)getPromoByBeaconMinor:(NSInteger) minor withMajor:(NSInteger)major andDistance:(NSString *)distance;

@end
