//
//  FRFaro.m
//  FaroDemo
//
//  Created by Jorge Leonardo Monge García on 03/08/14.
//  Copyright (c) 2014 Jorge Leonardo Monge García. All rights reserved.
//

#import "FRFaro.h"

@implementation FRFaro

+(NSDate *)getUTCFormateDateFromString:(NSString *)stringDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormatter dateFromString:stringDate];
    NSLog(@"Date actual UTC: %@", date);
    dateFormatter = nil;
    return date;
}

+ (NSString *)textForProximity:(CLProximity)proximity
{
    switch (proximity) {
        case CLProximityFar:
            return @"Far";
            break;
        case CLProximityNear:
            return @"Near";
            break;
        case CLProximityImmediate:
            return @"Near";
//            return @"Immediate";
            break;
            
        default:
//            return @"Far";
            return @"Unknown";
            break;
    }
}

#define FAR_DISTANCE      ((float) 1)
#define NEAR_MIN_DISTANCE ((float) 0)
#define NEAR_MAX_DISTANCE ((float) 1)

+ (NSString *)textForDistance:(float)distance
{
    
    if (distance > NEAR_MIN_DISTANCE && distance < NEAR_MAX_DISTANCE)
    {
        return @"Near";
    }
    
    if(distance >= FAR_DISTANCE)
        return @"Far";
    
    return @"Unknown";
    
}

@end
