//
//  FRFaro.h
//  FaroDemo
//
//  Created by Jorge Leonardo Monge García on 03/08/14.
//  Copyright (c) 2014 Jorge Leonardo Monge García. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ESTBeaconManager.h"

@interface FRFaro : NSObject


+ (NSDate *)getUTCFormateDateFromString:(NSString *)stringDate;
+ (NSString *)textForProximity:(CLProximity)proximity;
+ (NSString *)textForDistance:(float)distance;

@end
