//
//  WebServiceConnect.h
//  Bayer
//
//  Created by Jorge Leonardo Monge Garcia on 12/27/12.
//  Copyright (c) 2012 Jorge Leonardo Monge García. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WebServiceConnect : NSObject{
    NSMutableData* _receivedData;
    void (^ callbackBlock) (NSMutableData *Data, int type, int status, id sender);
    int _type;
    id _sender;
}

-(void)processRequestWhitURL:(NSString *)url sender:(id)sender requestType:(int)type andBlock:(void (^)(NSMutableData *Data, int type, int status, id sender))processResponse;

@end
