//
//  FRBaseRepository.m
//  FaroDemo
//
//  Created by Jorge Leonardo Monge García on 03/08/14.
//  Copyright (c) 2014 Jorge Leonardo Monge García. All rights reserved.
//

#import "FRBaseRepository.h"

@implementation FRBaseRepository

@synthesize managedObjectContext = _managedObjectContext;

-(id)initWithManagedObjectContext:(NSManagedObjectContext *) managedObjectContext
{
    self = [super init];
    
    if(self)
    {
        _managedObjectContext = managedObjectContext;
    }
    
    return self;
}

-(NSEntityDescription *)findOneByCriteria:(NSString *)criteria withValue:(id)value
{
    return [self findOne:[self getEntityName] ByCriteria:criteria withValue:value];
}

-(NSEntityDescription *)findOne:(NSString *)entity ByCriteria:(NSString *)criteria withValue:(id)value
{
    if(value == [NSNull null])
        return nil;
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription
                        entityForName:entity inManagedObjectContext:_managedObjectContext]];
    [request setFetchBatchSize:1];
    [request setFetchLimit:1];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"%@ = %@", criteria, value]];
    [request setPredicate:predicate];
    
    NSError *sqlError;
    NSArray *objetcs = [_managedObjectContext executeFetchRequest:request error:&sqlError];
    request          = nil;
    predicate        = nil;
    
    if(sqlError)
    {
        NSLog(@"%@", sqlError);
        return nil;
    }
    
    if([objetcs count] > 0)
        return [objetcs objectAtIndex:0];
    
    return nil;
}

-(NSArray *)findAll
{
    return [_managedObjectContext executeFetchRequest:[NSFetchRequest fetchRequestWithEntityName:[self getEntityName]] error:nil];
}

-(NSString *)getEntityName
{
    NSString *className = NSStringFromClass([self class]);
    className = [className stringByReplacingOccurrencesOfString:@"FR" withString:@""];
    className = [className stringByReplacingOccurrencesOfString:@"Repository" withString:@""];
    
    return className;
}

-(void)dealloc
{
//    NSLog(@"DEALLOCATED %@", [self getEntityName]);
    self.managedObjectContext = nil;
}

@end
