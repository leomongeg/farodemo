//
//  main.m
//  FaroDemo
//
//  Created by Jorge Leonardo Monge García on 22/06/14.
//  Copyright (c) 2014 Jorge Leonardo Monge García. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FRAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([FRAppDelegate class]));
    }
}
