//
//  FRRegionsManager.m
//  FaroDemo
//
//  Created by Jorge Leonardo Monge García on 20/07/14.
//  Copyright (c) 2014 Jorge Leonardo Monge García. All rights reserved.
//

#import "FRRegionsManager.h"
#import "ESTBeaconManager.h"
#import "FRBeaconRepository.h"
#import "Beacon.h"
#import "FRFaro.h"
#import "FRRecentlyBeaconRepository.h"

#define REGION_NOTIFICATION_DELAY 1

@interface FRRegionsManager () <ESTBeaconManagerDelegate>

@property (nonatomic, strong) ESTBeaconManager *beaconManager;
@property (nonatomic, strong) NSArray *regionsList;
@property (nonatomic, strong) FRRecentlyBeaconRepository *recentlyRepo;

@end

@implementation FRRegionsManager

@synthesize beaconManager = _beaconManager;
@synthesize regionsList = _regionsList;
@synthesize managedObjectContext = _managedObjectContext;
@synthesize recentlyRepo = _recentlyRepo;

-(id)init
{
    self = [super init];
    
    if(self)
    {
        _beaconManager = [[ESTBeaconManager alloc] init];
        [_beaconManager setDelegate:self];
        _recentlyRepo = [[FRRecentlyBeaconRepository alloc] initWithManagedObjectContext:_managedObjectContext];
//        [self initBeaconManager];
    }
    
    return self;
}

-(id)initWithManagedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
    self = [super init];
    
    if(self)
    {
        _managedObjectContext = managedObjectContext;
        _beaconManager = [[ESTBeaconManager alloc] init];
        [_beaconManager setDelegate:self];
        _recentlyRepo = [[FRRecentlyBeaconRepository alloc] initWithManagedObjectContext:_managedObjectContext];
//        [self initBeaconManager];
    }
    
    return self;
}

-(void)initBeaconManager
{
    [_beaconManager stopRangingBeaconsInAllRegions];

    for (Beacon *beacon in [self initialiceRegions])
    {
        NSLog(@"%@", [NSString stringWithFormat:@"beacon_region_%ld", [beacon.serverId integerValue]]);
        ESTBeaconRegion *region = [[ESTBeaconRegion alloc] initWithProximityUUID:[[NSUUID alloc]initWithUUIDString:beacon.uuid]
                                                                           major:[beacon.major integerValue]
                                                                           minor:[beacon.minor integerValue]
                                                                      identifier:[NSString stringWithFormat:@"beacon_region_%ld", [beacon.serverId integerValue]]];
//        region.notifyOnEntry = YES;
//        region.notifyOnExit  = YES;
        
        [_beaconManager stopMonitoringForRegion:region];
        [_beaconManager startRangingBeaconsInRegion:region];
        [_beaconManager startMonitoringForRegion:region]; // Necesario para las notificaciones ne background. Cambiar esto para el evento antes de que la app se cierre y hacer un stop monitoring.
//        [self.beaconManager startMonitoringForRegion:region];
        region = nil;
    }
    [_recentlyRepo deleteAllActivity];
    _regionsList = nil;
}


-(NSArray *)initialiceRegions
{
//    [self viewUserDefaultValues];
    
    if (_regionsList == nil)
    {
        FRBeaconRepository *repo = [[FRBeaconRepository alloc] initWithManagedObjectContext:_managedObjectContext];
        return [repo findAll];
        _regionsList = [repo findAll];
        repo         = nil;
    }
    
    
    /**
     * Cuando la aplicacion inicie, devera de obtener estos valores
     * de la base de datos para inicializar el monitoreo de las regiones.
     */
    if (_regionsList == nil)
    {
        NSUUID *uuid = [[NSUUID alloc]initWithUUIDString:@"B9407F30-F5F8-466E-AFF9-25556B57FE6D"];
        NSUUID *uuidv = [[NSUUID alloc]initWithUUIDString:@"08D4A950-80F0-4D42-A14B-D53E063516E6"];
        _regionsList = [[NSArray alloc] initWithObjects:
                        [[ESTBeaconRegion alloc] initWithProximityUUID: uuid major:32795 minor:12381 identifier:@"Blueberry Pie"],
                        [[ESTBeaconRegion alloc] initWithProximityUUID: uuid major:60806 minor:6366 identifier:@"Icy Marshmallow"],
                        [[ESTBeaconRegion alloc] initWithProximityUUID: uuidv major:61001 minor:13001 identifier:@"Virtual"]
                        , nil];
    }
    
    return _regionsList;
}

#pragma mark - ESTBeaconManager delegate

//-(void)beaconManager:(ESTBeaconManager *)manager didEnterRegion:(ESTBeaconRegion *)region
//{
//    NSLog(@"Entro en la region %@, %ld | %ld", region.identifier, [region.major integerValue], [region.minor integerValue]);
//    [_beaconManager startRangingBeaconsInRegion:region];
//}

-(void)beaconManager:(ESTBeaconManager *)manager didExitRegion:(ESTBeaconRegion *)region
{
//    [_beaconManager stopRangingBeaconsInRegion:region];
    
    NSDictionary *message = [[NSDictionary alloc] initWithObjectsAndKeys:
                             region     , @"region",
                             manager    , @"manager", nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"beaconExitRegionNotification"
                                                        object:message
                                                      userInfo:nil];
    message = nil;
//    NSLog(@"SALIO DE LA REGION");
}

- (void)beaconManager:(ESTBeaconManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(ESTBeaconRegion *)region
{
//    NSLog(@"Cantidad de Beacons %ld", beacons.count);
    if (beacons.count > 0)
    {
        ESTBeacon *firstBeacon = [beacons firstObject];
//        NSLog(@"Distancia %.2f", [firstBeacon.distance floatValue]);
        if([firstBeacon.distance floatValue] < 0)
            return;
        
        NSDictionary *message = [[NSDictionary alloc] initWithObjectsAndKeys:firstBeacon, @"beacon",
                                                                             region     , @"region",
                                                                             manager    , @"manager", nil];
        
//        [self checkForLastActivityFromDBInRegion:region withBeacon:firstBeacon];
        
//        if([self checkForLastActivityInRegion:region withBeacon:firstBeacon])
        
        if ([self checkForLastActivityFromDBInRegion:region withBeacon:firstBeacon])
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"beaconNotification"
                                                                object:message
                                                              userInfo:nil];
        }
//        NSLog(@"Distancia Real ---> %@", [self textForProximity:firstBeacon.proximity]);
        message = nil;

//        NSLog(@"Region: %@", region.identifier);
//        NSLog(@"Becon major: %@", firstBeacon.major);
//        NSLog(@"Becon minor: %@", firstBeacon.minor);
//        NSLog(@"Becon proximidad: %@", [self textForProximity:firstBeacon.proximity]);
    }
}

- (NSString *)textForProximity:(CLProximity)proximity
{
    switch (proximity) {
        case CLProximityFar:
            return @"Far";
            break;
        case CLProximityNear:
            return @"Near";
            break;
        case CLProximityImmediate:
            return @"Immediate";
            break;
            
        default:
            return @"Unknown";
            break;
    }
}

-(Boolean)checkForLastActivityFromDBInRegion:(ESTBeaconRegion *)region withBeacon:(ESTBeacon *)beacon
{
    Boolean result           = true;
    NSString *serverIdStr    = [region.identifier stringByReplacingOccurrencesOfString:@"beacon_region_" withString:@""];
    NSInteger beaconServerId = [serverIdStr integerValue];
    NSString *key            = [NSString stringWithFormat:@"%@%d%d_%@", region.identifier, [beacon.major unsignedIntValue], [beacon.minor unsignedIntValue], [FRFaro textForDistance:[beacon.distance floatValue]]];
    
//    if([serverIdStr isEqualToString:@"2"])
//    {
//        NSLog(@"Proximidad: %@, distancia: %f", [self textForProximity:beacon.proximity], [beacon.distance doubleValue]);
//    }
    
    if(![_recentlyRepo verifyActivity:beacon andServerId:beaconServerId andKey:(NSString *)key])
    {
        result = false;
    }
    
    return result;
}

-(Boolean)checkForLastActivityInRegion:(ESTBeaconRegion *)region withBeacon:(ESTBeacon *)beacon
{
    NSString *key                  = [NSString stringWithFormat:@"%@%d%d_%@", region.identifier, [beacon.major unsignedIntValue], [beacon.minor unsignedIntValue], [FRFaro textForProximity:beacon.proximity]];
    NSUserDefaults *myUserDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *lastActivity     = [myUserDefaults objectForKey:key];
    NSTimeInterval now             = [[NSDate date] timeIntervalSince1970];
    
    if(lastActivity != nil)
    {
        
        NSTimeInterval last  = [[lastActivity objectForKey:@"timestamp"] doubleValue];

        NSInteger minutes   = floor((now - last)/60);

        if(minutes < REGION_NOTIFICATION_DELAY && REGION_NOTIFICATION_DELAY != 0)
            return false;
        
    }else{
        lastActivity = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[[NSDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithDouble:[[NSDate date] timeIntervalSince1970]],  @"timestamp", nil], region.identifier, nil];
    }
    
    [myUserDefaults setObject:[[NSDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithDouble:now], @"timestamp", nil] forKey:key];
    key = nil;
    
    return true;
}

-(void)viewUserDefaultValues
{
    NSUserDefaults * defs = [NSUserDefaults standardUserDefaults];
    NSDictionary * dict   = [defs dictionaryRepresentation];
    for (id key in dict)
    {
        NSLog(@"%@", key);
        NSDictionary *lastActivity = [dict objectForKey:key];
        if ([lastActivity isKindOfClass:[NSDictionary class]])
        {
            if([lastActivity objectForKey:@"timestamp"] != nil)
            {
                NSTimeInterval now   = [[NSDate date] timeIntervalSince1970];
                NSTimeInterval last  = [[lastActivity objectForKey:@"timestamp"] doubleValue];
                NSInteger minutes    = floor((now - last)/60);
                
                if(minutes >= REGION_NOTIFICATION_DELAY && REGION_NOTIFICATION_DELAY != 0)
                {
                    [defs removeObjectForKey:key];
                }
            }
        }
        
    }
    [defs synchronize];
}

-(void)dealloc
{
    _managedObjectContext = nil;
    _recentlyRepo = nil;
}

@end
