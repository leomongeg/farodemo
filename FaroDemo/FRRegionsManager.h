//
//  FRRegionsManager.h
//  FaroDemo
//
//  Created by Jorge Leonardo Monge García on 20/07/14.
//  Copyright (c) 2014 Jorge Leonardo Monge García. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ESTBeaconRegion.h"

@interface FRRegionsManager : NSObject

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

- (id)init;
- (id)initWithManagedObjectContext:(NSManagedObjectContext *) managedObjectContext;
- (void)initBeaconManager;


@end


