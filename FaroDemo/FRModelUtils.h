//
//  FRModelUtils.h
//  FaroDemo
//
//  Created by Jorge Leonardo Monge García on 03/08/14.
//  Copyright (c) 2014 Jorge Leonardo Monge García. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FRModelUtils : NSObject

+(NSEntityDescription *)findOne:(NSString *)entity ByCriteria:(NSString *)criteria withValue:(id)value inMOC:(NSManagedObjectContext *) managedObjectContext;

@end
