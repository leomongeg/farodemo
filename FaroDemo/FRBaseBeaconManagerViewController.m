//
//  FRBaseBeaconManagerViewController.m
//  FaroDemo
//
//  Created by Jorge Leonardo Monge García on 20/07/14.
//  Copyright (c) 2014 Jorge Leonardo Monge García. All rights reserved.
//

#import "FRBaseBeaconManagerViewController.h"

@interface FRBaseBeaconManagerViewController ()

@end

@implementation FRBaseBeaconManagerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"beaconNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(beaconNotification:)
                                                 name:@"beaconNotification"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"beaconExitRegionNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(beaconExitRegionNotification:)
                                                 name:@"beaconExitRegionNotification"
                                               object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)beaconNotification:(NSNotification *)notification
{
    NSDictionary *options = (NSDictionary *)[notification object];
    [self didRangeBeacon:[options objectForKey:@"beacon"] inRegion:[options objectForKey:@"region"] andBeaconManager:[options objectForKey:@"manager"]];
}

- (void)beaconExitRegionNotification:(NSNotification *)notification
{
    NSDictionary *options = (NSDictionary *)[notification object];
    [self didBeaconManager:[options objectForKey:@"manager"] didExitRegion:[options objectForKey:@"region"]];
}

-(void)didRangeBeacon:(ESTBeacon *)beacon inRegion:(ESTBeaconRegion *)region andBeaconManager:(ESTBeaconManager *)manager
{
    NSAssert(NO, @"This is an abstract method and should be overridden");
}


-(void)didBeaconManager:(ESTBeaconManager *)manager didExitRegion:(ESTBeaconRegion *)region
{
    NSAssert(NO, @"This is an abstract method and should be overridden");
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
