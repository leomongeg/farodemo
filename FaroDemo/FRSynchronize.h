//
//  FRSynchronize.h
//  FaroDemo
//
//  Created by Jorge Leonardo Monge García on 03/08/14.
//  Copyright (c) 2014 Jorge Leonardo Monge García. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FRSynchronize : NSObject

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

-(id)initWithManagedObjectContext:(NSManagedObjectContext *)managedObjectContext;

@end
