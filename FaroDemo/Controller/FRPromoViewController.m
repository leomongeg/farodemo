//
//  FRPromoViewController.m
//  FaroDemo
//
//  Created by Jorge Leonardo Monge García on 09/08/14.
//  Copyright (c) 2014 Jorge Leonardo Monge García. All rights reserved.
//

#import "FRPromoViewController.h"
#import "FRAppDelegate.h"
#import <AudioToolbox/AudioServices.h>
#import "Promo.h"
#import "FRPromoRepository.h"
#import "FRFaro.h"


@interface FRPromoViewController ()

@end

@implementation FRPromoViewController

@synthesize managedObjectContext = _managedObjectContext;
@synthesize lblTitle = _lblTitle;
@synthesize lblDescription = _lblDescription;
@synthesize imgPromo = _imgPromo;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    FRAppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    _managedObjectContext      = [appDelegate managedObjectContext];
    [self setHomePromo];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)didRangeBeacon:(ESTBeacon *)beacon inRegion:(ESTBeaconRegion *)region andBeaconManager:(ESTBeaconManager *)manager
{
    NSLog(@"TENGO LA NOTIFICACION de la region: %@", region.identifier);
    
    FRPromoRepository *repoPromo = [[FRPromoRepository alloc]initWithManagedObjectContext:_managedObjectContext];
    
    Promo *promo = [repoPromo getPromoByBeaconMinor:[beacon.minor integerValue] withMajor:[beacon.major integerValue] andDistance:[FRFaro textForDistance:[beacon.distance floatValue]]];
    
    NSLog(@"promo: %@", promo.title);
    if(promo)
    {
        UIImage *promoImg = [UIImage imageNamed:promo.image1];
        [_imgPromo setImage:promoImg];
        [_lblTitle setText:promo.title];
        [_lblDescription setText:promo.descriptionStr];
        promoImg = nil;
    }
    
    if([promo.type isEqualToString:@"near"])
    {
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    }
    
    promo     = nil;
    repoPromo = nil;

}

-(void)didBeaconManager:(ESTBeaconManager *)manager didExitRegion:(ESTBeaconRegion *)region
{
    [self setHomePromo];
}

-(void)setHomePromo
{
    UIImage *promoImg = [UIImage imageNamed:@"IMG-004.jpg"];
    [_imgPromo setImage:promoImg];
    [_lblTitle setText:@"¡Lo Que Busco!"];
    [_lblDescription setText:@"La mejor forma de recibir información sobre mis tiendas favoritas y sus ofertas"];
    promoImg = nil;
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

//-(void)updateScreenPromo:()

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
