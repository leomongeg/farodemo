//
//  FRPromoViewController.h
//  FaroDemo
//
//  Created by Jorge Leonardo Monge García on 09/08/14.
//  Copyright (c) 2014 Jorge Leonardo Monge García. All rights reserved.
//

#import "FRBaseBeaconManagerViewController.h"

@interface FRPromoViewController : FRBaseBeaconManagerViewController

@property (weak, nonatomic) IBOutlet UIImageView *imgPromo;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@end
