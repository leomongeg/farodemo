//
//  FRTabBarController.h
//  FaroDemo
//
//  Created by Jorge Leonardo Monge García on 12/08/14.
//  Copyright (c) 2014 Jorge Leonardo Monge García. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FRTabBarController : UITabBarController

@end
