//
//  FRProximityViewController.m
//  FaroDemo
//
//  Created by Jorge Leonardo Monge García on 17/08/14.
//  Copyright (c) 2014 Jorge Leonardo Monge García. All rights reserved.
//

#import "FRProximityViewController.h"
#import "ESTBeaconManager.h"
#import "FRFaro.h"

@interface FRProximityViewController () <ESTBeaconManagerDelegate>

@property (nonatomic, strong) ESTBeacon         *beacon;
@property (nonatomic, strong) ESTBeaconManager  *beaconManager;
@property (nonatomic, strong) ESTBeaconRegion   *beaconRegion;

@property (nonatomic, strong) UIImageView       *positionDot;
@property (weak, nonatomic) IBOutlet UILabel *lblDistance;

@end

#define MAX_DISTANCE 20
#define TOP_MARGIN   150

@implementation FRProximityViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.positionDot = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"dotImage"]];
    [self.positionDot setCenter:self.view.center];
    [self.view addSubview:self.positionDot];
    
    /*
     * BeaconManager setup.
     */
    self.beaconManager = [[ESTBeaconManager alloc] init];
    self.beaconManager.delegate = self;
    
    self.beaconRegion = [[ESTBeaconRegion alloc] initWithProximityUUID: [[NSUUID alloc]initWithUUIDString:@"B9407F30-F5F8-466E-AFF9-25556B57FE6D"] major:32795 minor:12381 identifier:@"DistancerDemoRegion"];
    
//    self.beaconRegion = [[ESTBeaconRegion alloc] initWithProximityUUID: [[NSUUID alloc]initWithUUIDString:@"B9407F30-F5F8-466E-AFF9-25556B57FE6D"] major:60806 minor:6366 identifier:@"DistancerDemoRegion"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)startMonitoring
{
    [self.beaconManager startRangingBeaconsInRegion:self.beaconRegion];
}

-(void)stopMonitoring;

{
    [self.beaconManager stopRangingBeaconsInRegion:self.beaconRegion];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self startMonitoring];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [self stopMonitoring];
}


#pragma mark - ESTBeaconManager delegate

- (void)beaconManager:(ESTBeaconManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(ESTBeaconRegion *)region
{
    ESTBeacon *firstBeacon = [beacons firstObject];

    [self updateDotPositionForDistance:[firstBeacon.distance floatValue] andProximity:[FRFaro textForProximity:firstBeacon.proximity]];
}

#pragma mark -

- (void)updateDotPositionForDistance:(float)distance andProximity:(NSString *)proximity
{
//    NSLog(@"distance: %f", distance);
    
    if([proximity isEqualToString:@"Unknown"])
    {
        [_lblDistance setText:@"+ 20 m"];
        int newY = self.view.frame.size.height - 100;
        [self.positionDot setCenter:CGPointMake(self.positionDot.center.x, newY)];
        
        return;
    }
    
    float step = (self.view.frame.size.height - TOP_MARGIN) / MAX_DISTANCE;
    
    int newY = TOP_MARGIN + (distance * step);
    
    [_lblDistance setText:[NSString stringWithFormat:@"%.2f m", distance]];
    
//    if(distance < 1)
//    {
//        [_lblDistance setText:@"- 1 m"];
//    }
    
    if(distance <= 0)
    {
        newY = self.view.frame.size.height - 100;
        [_lblDistance setText:@"+ 20 m"];
    }
    
    [self.positionDot setCenter:CGPointMake(self.positionDot.center.x, newY)];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
