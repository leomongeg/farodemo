//
//  FRBaseBeaconManagerViewController.h
//  FaroDemo
//
//  Created by Jorge Leonardo Monge García on 20/07/14.
//  Copyright (c) 2014 Jorge Leonardo Monge García. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ESTBeaconManager.h"

@interface FRBaseBeaconManagerViewController : UIViewController

- (void)beaconNotification:(NSNotification *)notification;
- (void)beaconExitRegionNotification:(NSNotification *)notification;

-(void)didRangeBeacon:(ESTBeacon *)beacon inRegion:(ESTBeaconRegion *)region andBeaconManager:(ESTBeaconManager *)manager;
-(void)didBeaconManager:(ESTBeaconManager *)manager didExitRegion:(ESTBeaconRegion *)region;

@end

